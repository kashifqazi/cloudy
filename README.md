# cloudy

## TO INSTALL

Clone from git

From the cloudy directory:

chmod 700 install.sh

chmod 700 workload.sh

### sudo ./install.sh

## TO RUN

### sudo ./workload.sh HostName [-m*MaxMemoryToUse* ] [ -t*TimePeriodSeconds*]

e.g. To run a workload based on AHost75, using up to 8GB of RAM, and at 10 minute intervals

sudo ./workload.sh AHost75 -m8 -t600

e.g. To run a workload based on GHost75, using up to 8GB of RAM, and at default intervals

sudo ./workload.sh GHost75 -m8

## TO VIEW

Visit the link 

machine_ip_address/home.php

(Port 80 should be accessible)
