#!/bin/bash

mem="16"
scale="300"
for argu in "$@"
do
  ar=${argu:0:2}
  if [ $ar == "-m" ]
  then
    mem=${argu:2}
  elif [ $ar == "-t" ]
  then
    scale=${argu:2}
  fi
done
input="traces/"$1
#mem=$2
#scale=$3
count=0
echo "Host:$1" > config
echo "Mem:$mem" >> config
echo "Scale:$scale" >> config

cluster=$(echo $1 | cut -c1-1)
trace=${1:1}
if [ $cluster == 'A' ]
then
	wget https://gitlab.com/kashifqazi/googletraces/-/raw/master/AlibabaTraces/$trace
  mv $trace traces/$1
else
	wget https://gitlab.com/kashifqazi/googletraces/-/raw/master/GoogleTraces/G$trace
  mv G$trace traces/$1
fi



python3 traceaggr.py $1
sudo cp *.png /var/www/html/
sudo cp aggr /var/www/html/

atop/atopsar -c -m 5 150000 > logs &
while IFS= read -r line
do
  cpu=$(echo $line | cut -d"," -f2)
  me=$(echo $line | cut -d"," -f3)

  #cpu=$(bc -l <<<"${cp}*100")
  #mem=$(bc -l <<<"${me}*100")
  echo "$cpu"
  echo "$me"
  #count=`expr $count + 1`
  #if [ $count -ge 2 ]
  #then
  #  break
  #fi
  memory=$(bc -l <<<"${me}*${mem}*1024*1024*1024/100")
  unit="b"
  #atop/atopsar -c -m 5 >> logs &
  toppid=$!
#  eval "sudo cpulimit -i -l $cpu stress-ng/stress-ng --vm 1 --vm-bytes $memory$unit -t $scale --times --metrics-brief --perf --log-brief >> stats"
  stress-ng/stress-ng --vm 1 --vm-bytes $memory$unit -t $scale --times --metrics-brief --perf --log-brief >> stats &
  stresspid=$!
  sleep 5
  eval "sudo cpulimit -i -l $cpu -p $stresspid"
#  sleep $scale
#  kill -9 $stresspid
#  kill -9 $toppid
#  echo "Interval\n" >> logs
   echo "\n" >> stats
  #cat stats >> statscomplete
done < "$input"

